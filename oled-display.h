#ifndef _OLED_DISPLAY_H
#define _OLED_DISPLAY_H

#include "ESP8266_E131_NeoPixel.h"
#include <ESP8266WiFi.h>        //ESP8266 Core WiFi Library
#include <WiFiManager.h>

/* OLED Librarys */
#include <Wire.h>
#include <SSD1306Wire.h>            /*  https://github.com/squix78/esp8266-oled-ssd1306 */
#include <OLEDDisplayUi.h>      /*  */
#include "images.h"

// Pin definitions for I2C OLED
#define OLED_SDA    D2          // pin 14
#define OLED_SDC    D1          // pin 13
#define OLED_ADDR   0x3C

extern const char VERSION[];
extern char univ_conf[5];
extern char start_led[3];
extern WiFiManager wifiManager;

void initOled();
void msOverlay(OLEDDisplay * display, OLEDDisplayUiState * state);
void drawFrame1(OLEDDisplay * display, OLEDDisplayUiState * state, int16_t x, int16_t y);
void drawFrame2(OLEDDisplay * display, OLEDDisplayUiState * state, int16_t x, int16_t y);
void drawFrame3(OLEDDisplay * display, OLEDDisplayUiState * state, int16_t x, int16_t y);

#endif
