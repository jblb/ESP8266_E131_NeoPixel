
#include "oled-display.h"

// SSD1306 display
SSD1306Wire display(OLED_ADDR, OLED_SDA, OLED_SDC); // For I2C
OLEDDisplayUi ui(&display);

// this array keeps function pointers to all frames
// frames are the single views that slide from right to left
FrameCallback frames[] = { drawFrame1, drawFrame2, drawFrame3 };

// how many frames are there?
int frameCount = 3;

OverlayCallback overlays[] = { msOverlay };

int overlaysCount = 1;

/* *********************************************************************************************** */
void initOled()
{
    ui.setTargetFPS(15);

    //ui.setTimePerFrame(20);
    ui.disableAutoTransition();

    ui.setActiveSymbol(activeSymbol);
    ui.setInactiveSymbol(inactiveSymbol);

    // You can change this to
    // TOP, LEFT, BOTTOM, RIGHT
    ui.setIndicatorPosition(BOTTOM);

	  // Disable drawing of indicators.
	  ui.disableIndicator();

    // Defines where the first frame is located in the bar.
    ui.setIndicatorDirection(LEFT_RIGHT);

    // You can change the transition that is used
    // SLIDE_LEFT, SLIDE_RIGHT, SLIDE_TOP, SLIDE_DOWN
    ui.setFrameAnimation(SLIDE_LEFT);

    // Add frames
    ui.setFrames(frames, frameCount);

    // Add overlays
    ui.setOverlays(overlays, overlaysCount);

    // Inital UI takes care of initalising the display too.
    ui.init();

    display.flipScreenVertically();
    ui.update();
}

void msOverlay(OLEDDisplay * display, OLEDDisplayUiState * state)
{
    display->setTextAlignment(TEXT_ALIGN_RIGHT);
    display->setFont(ArialMT_Plain_10);
    display->drawString(128, 0, WiFi.localIP().toString());
    display->setTextAlignment(TEXT_ALIGN_LEFT);
    display->drawString(0, 0, (String) ESP.getChipId());
    display->setTextAlignment(TEXT_ALIGN_RIGHT);
    display->drawString(118, 50, "v" + (String) VERSION);
}

void drawFrame1(OLEDDisplay * display, OLEDDisplayUiState * state,
                int16_t x, int16_t y)
{
    // draw an xbm image.
    // Please note that everything that should be transitioned
    // needs to be drawn relative to x and y

    // if this frame need to be refreshed at the targetFPS you need to
    // return true
    display->drawXbm(x + 34, y + 14, WiFi_Logo_width, WiFi_Logo_height,
                     WiFi_Logo_bits);
}

void drawFrame2(OLEDDisplay * display, OLEDDisplayUiState * state,
                int16_t x, int16_t y)
{
    /*
     * config Mode
     *
     */
    display->setFont(ArialMT_Plain_10);


    display->setTextAlignment(TEXT_ALIGN_LEFT);
    display->drawString(0 + x, 11 + y, "Config mode ");
    display->setTextAlignment(TEXT_ALIGN_LEFT);
    display->drawString(0 + x, 23, "SSID: "+ (String)WiFi.SSID());

}

void drawFrame3(OLEDDisplay * display, OLEDDisplayUiState * state,
                int16_t x, int16_t y)
{
    /*
     * Runing mode
     *
     */
    display->setFont(ArialMT_Plain_10);


    display->setTextAlignment(TEXT_ALIGN_LEFT);
    display->drawString(0 + x, 11 + y, "Connected to: "+ (String)WiFi.SSID());
    display->setTextAlignment(TEXT_ALIGN_LEFT);
    display->drawString(0 + x, 23, "Universe: " + (String)univ_conf);
    display->drawString(0 + x, 35, "Addr "+ (String)start_led);
}

/* ****************************************************************************************** */
