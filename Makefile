
ARDUINO_BIN ?= ~/bin/arduino
TARGET = ESP8266_E131_NeoPixel


all: verify

verify:
	$(ARDUINO_BIN) -v --verify --board esp8266:esp8266:d1_mini \
	--pref build.path=build \
	--pref target_package=esp8266 \
	--pref target_platform=esp8266 \
	--pref board=d1_mini \
	--pref custom_CpuFrequency=d1_mini_160 \
	$(TARGET).ino

upload:
	$(ARDUINO_BIN) -v --upload --board esp8266:esp8266:d1_mini \
	--preserve-temp-files \
	--pref build.path=build \
	--pref target_package=esp8266 \
	--pref target_platform=esp8266 \
	--pref board=d1_mini \
	--pref custom_CpuFrequency=d1_mini_160 \
	$(TARGET).ino

indent:
	indent -kr --no-tabs *.ino *.cpp *.h

clean:
	rm -f *~
	rm -R build

mrproper: clean
