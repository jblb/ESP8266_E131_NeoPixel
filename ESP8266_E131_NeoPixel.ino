/*
* ESP8266_NeoPixel.ino - Simple sketch to listen for E1.31 data on an ESP8266
*                        and drive WS2811 LEDs using the NeoPixel Library
*
* == Requires Adafruit_NeoPixel - http://github.com/adafruit/Adafruit_NeoPixel
*
* Project: E131 - E.131 (sACN) library for Arduino
* Copyright (c) 2015 Shelby Merrick
* http://www.forkineye.com
*
*  This program is provided free for you to use in any way that you wish,
*  subject to the laws and regulations where you are using it.  Due diligence
*  is strongly suggested before using this code.  Please give credit where due.
*
*  The Author makes no warranty of any kind, express or implied, with regard
*  to this program or the documentation contained in this document.  The
*  Author shall not be liable in any event for incidental or consequential
*  damages in connection with, or arising out of, the furnishing, performance
*  or use of these programs.
*
*/

#include <ESP8266WiFi.h>        //ESP8266 Core WiFi Library
#include <DNSServer.h>          //Local DNS Server used for redirecting all requests to the configuration portal
#include <ESP8266WebServer.h>   //Local WebServer used to serve the configuration portal
#include <WiFiManager.h>        //https://github.com/tzapu/WiFiManager WiFi Configuration Manager

#include <ESPAsyncE131.h>
#include <Adafruit_NeoPixel.h>
#include "ESP8266_E131_NeoPixel.h"
#include "config.h"             /* change values and rename config.h.sample */

#define NUM_PIXELS 170          /* Number of pixels */
#define UNIVERSE 1              /* Universe to listen for */
#define UNIVERSE_COUNT 2        /* Total number of Universes to listen for, starting at UNIVERSE */

#define CHANNEL_START 1         /* Channel to start listening at */
#define DATA_PIN D3             /* Pixel output - GPIO0 */

//for LED status
#include <Ticker.h>
Ticker ticker;

#include "oled-display.h"

// ESPAsyncE131 instance with UNIVERSE_COUNT buffer slots
ESPAsyncE131 e131(UNIVERSE_COUNT);

// Leds
Adafruit_NeoPixel pixels =
Adafruit_NeoPixel(NUM_PIXELS, DATA_PIN, NEO_GRB + NEO_KHZ800);
// web_interface config variables
char univ_conf[5] = "2";
char start_led[3] = "1";
char nb_leds[5] = "12";
void tick()
{
    //toggle state
    int state = digitalRead(BUILTIN_LED);       // get the current state of LED pin
    digitalWrite(BUILTIN_LED, !state);  // set pin to the opposite state
}

void configModeCallback(WiFiManager * myWiFiManager)
{
    Serial.println("Entered config mode");
    Serial.println(WiFi.softAPIP());
    //if you used auto generated SSID, print it
    Serial.println(myWiFiManager->getConfigPortalSSID());
    ui.switchToFrame(1);
    ui.update();

}

void setup()
{
    /* Initial pin states */
    pinMode(DATA_PIN, OUTPUT);
    digitalWrite(DATA_PIN, LOW);

    Serial.begin(115200);
    delay(10);
    //set led pin as output
    pinMode(BUILTIN_LED, OUTPUT);
    // init display
    initOled();
    ui.switchToFrame(0);
    ui.update();

    /* Extra parameters on config page */

    WiFiManagerParameter custom_universe("universe", "Nb Universe", univ_conf, 5);
    WiFiManagerParameter custom_led_start("led_start", "1st Led Addr", start_led, 3);
    WiFiManagerParameter custom_nb_led("nb_led", "Nb Led", nb_leds, 5);

    /* **************************** */

    /* initialise wifimanager */
    WiFiManager wifiManager;
    /* No debug output */
    wifiManager.setDebugOutput(false);
    //set callback that gets called when connecting to previous WiFi fails, and enters Access Point mode
    wifiManager.setAPCallback(configModeCallback);
    //add all your parameters here
    wifiManager.addParameter(&custom_universe);
    wifiManager.addParameter(&custom_led_start);
    wifiManager.addParameter(&custom_nb_led);

    //reset settings - for testing
    //wifiManager.resetSettings();

    // start ticker with 0.5 because we start in AP mode and try to connect
    ticker.attach(0.5, tick);

    if (!wifiManager.autoConnect()) {
        Serial.println("failed to connect and hit timeout");
        //reset and try again, or maybe put it to deep sleep
        ESP.reset();
        delay(1000);
    }
    // here you have connected to the WiFi, so let's go !
    Serial.println();
    Serial.println("connected...yeey :)");
    /* ****************************** */

    strcpy(nb_leds,custom_nb_led.getValue());
    strcpy(univ_conf, custom_universe.getValue());
    strcpy(start_led, custom_led_start.getValue());
    Serial.print("Universe: ");
    Serial.println(univ_conf);
    Serial.print("starting LED addr: ");
    Serial.println(start_led);

    Serial.println(WiFi.localIP());

    // OLED display frame 2
    ui.switchToFrame(2);
    ui.update();


    /* ============================== */
    /* Choose one to begin listening for E1.31 data */
    // e131.begin(ssid, passphrase);                                            /* via Unicast on the default port */
    // e131.beginMulticast(ssid, passphrase, UNIVERSE);         /* via Multicast for Universe 1 */
    if (e131.begin(E131_MULTICAST, UNIVERSE, UNIVERSE_COUNT))   // Listen via Multicast
      Serial.println(F("Listening for data..."));
    else
        Serial.println(F("*** e131.begin failed ***"));

  /* Initialize output */
    pixels.begin();
    pixels.show();
}

void loop()
{   ui.update();


    /* Parse a packet and update pixels */
    if (!e131.isEmpty()) {
      e131_packet_t packet;
      e131.pull(&packet);     // Pull packet from ring buffer
 /*
 *              htons(packet.universe),                 // The Universe for this packet
                htons(packet.property_value_count) - 1, // Start code is ignored, we're interested in dimmer data
                e131.stats.num_packets,                 // Packet counter
                e131.stats.packet_errors,               // Packet error counter
                packet.property_values[1]);             // Dimmer data for Channel 1
 */

       if (e131.stats.num_packets) {
          if (htons(packet.universe) == UNIVERSE) {
              for (int i = 0; i < atoi(nb_leds); i++) {
                   int j = i * 3 + (CHANNEL_START );
                   pixels.setPixelColor(i, packet.property_values[j], packet.property_values[j + 1],
                                     packet.property_values[j + 2]);
              }
              pixels.show();
              /*
               * Serial.print("\n");
              Serial.print("Universe ");
              Serial.print(htons(packet.universe));
              Serial.print(" / ");
              Serial.print(e131.stats.num_packets);
              Serial.print(" Channels | Packets: ");
              Serial.print(e131.stats.num_packets);
              Serial.print(" / Sequence Errors: ");
              Serial.print(e131.stats.packet_errors);
              Serial.print (" Data[1]: ");
              Serial.print (packet.property_values[1]);
              Serial.print("\n\r");
              */

          }
       }
    }
}
